const AWS = require('aws-sdk');

const s3 = new AWS.S3();
const dynamoDb = new AWS.DynamoDB.DocumentClient();

const tableName = "ihs-evidence";

exports.handler = async (event) => {
    
    const reference = event.reference;
    
    const queryParams = { 
        TableName: tableName,
        ExpressionAttributeValues: {
            ':ref': reference
        },
        KeyConditionExpression: 'bsaReference = :ref'
    };
    
    var records = await dynamoDb.query(queryParams).promise();
    var evidenceRecord = records.Items[0];
    
    
    var preSignedLinkExpiry = process.env.PRESIGNED_URL_EXPIRY_SECONDS*1;
    
    var response;
    if(evidenceRecord) {
        console.log("record found", evidenceRecord.bsaReference);
        
        var filesWithLink = [];
        var file;
        for(file of evidenceRecord.files) {
            // generate signed url
            var linkParams = {Bucket: process.env.BUCKET_NAME, Key: evidenceRecord.bsaReference + "/" + file.filename, Expires: preSignedLinkExpiry};
            var presignedUrl = s3.getSignedUrl('getObject', linkParams);
            
            filesWithLink.push({
                id: file.fileId,
                addedDate: new Date(file.addedDate).toLocaleString('en-GB', { dateStyle: 'medium', timeStyle: 'medium'}),
                filename: file.filename,
                fileType: file.fileType,
                link: presignedUrl
            }
            );
        }
        
        response = {
            filesCount: evidenceRecord.files.length,
            files: filesWithLink
        };
    }
    
    return response;
};
